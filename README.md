# gitlab-releaser-arm64

Simple image that contains the AWS CLI and Gitlab release-cli [https://gitlab.com/gitlab-org/release-cli] to assist with uploading artifacts to S3 + making releases in other projects.

Instructions and PGP for AWS CLI from https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html#v2-install-linux-validate
